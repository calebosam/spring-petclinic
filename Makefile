IMAGE = caleb9083/spring-petclinic

build: 
	docker build -t $(IMAGE) .

push:
	docker push $(IMAGE)
up:
	docker run -p 4000:8080 --name spring-petclinic $(IMAGE)

down: 
	docker rm -f spring-petclinic
	
compose-build:
	docker-compose -f docker-compose.dev.yml build

compose-up:
	docker-compose -f docker-compose.dev.yml up
